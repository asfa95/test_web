import React, { Component } from 'react';
import { PropTypes } from 'prop-types'; 
import { observer } from 'mobx-react';

const Item = observer(class Item extends Component {
  static propTypes = {
    todo: PropTypes.object.isRequired
  };

  constructor() {
    super();

    this.handleRemove = this.handleRemove.bind(this);
  }

  addDefaultSrc(ev){
    ev.target.src = 'https://www.publicdomainpictures.net/pictures/280000/nahled/not-found-image-15383864787lu.jpg'
  }

  getThumbnail(){
    let imgs = this.props.todo.images
    var img = ''
    if(imgs.length>0){
       if(imgs[0].image) img = imgs[0].image;
    }
    return img
  }

  render() {
    return (
        <div className="card">
          <img onError={this.addDefaultSrc} className="card-img-top" src={this.getThumbnail()}  alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{this.props.todo.name}</h5>
            <div className="card-text">  
              <p className='text-primary'> {'Rp. '+this.props.todo.price} </p>
            </div>
            <div> <button onClick={this.handleRemove} className="btn btn-sm btn-danger"> Hapus </button> </div>
          </div>
        </div>
    );
  }

  handleRemove() {
    this.props.store.deleteProducts(this.props.todo);
  }
});

export default Item;
