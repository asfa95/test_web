const config = require('./../config/config.js')
const token = config.webAPI.token
const webApiUrl = config.webAPI.url
console.log(config)
class serviceAPI {
    get = async (url,urlParams) => {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("token", token);
        const options = {
            method: "GET",
            headers
        }
     const request = new Request(webApiUrl +url+ "?" + urlParams, options);
     const response = await fetch(request);
     return response.json();
    }
    post = async (model) => {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("token", token);
        var options = {
            method: "POST",
            headers,
            body: JSON.stringify(model)
        }
        const request = new Request(webApiUrl, options);
        const response = await fetch(request);
        return response.json();
    }
    put = async (model) => {
        const headers = new Headers()
        headers.append("Content-Type", "application/json");
        headers.append("token", token);
        var options = {
            method: "PUT",
            headers,
            body: JSON.stringify(model)
        }
        const request = new Request(webApiUrl, options);
        const response = await fetch(request);
        return response.json();
    }
    delete = async (url,id) => {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("token", token);
        const options = {
            method: "DELETE",
            headers
        }
        const request = new Request(webApiUrl+url+'/'+id, options);
        const response = await fetch(request);
        return response.json();
    }
}
export default serviceAPI;