import { extendObservable,runInAction } from 'mobx';
import serviceAPI from './../API/Service'

export class TodoStore {

  constructor() {
    extendObservable(this, {
      productList: [],
      productResponse:[],
      isLoading: false,
      page:1,
      get total() {
        return this.productList.length;
      }
    });
    this.serviceAPI = new serviceAPI();
  }

  load(params) {
    this.emptyItems();
    this.startLoading();
  }

  getProducts = async (page) => {
       try {
           var params = {
               page: page
           };
           const urlParams = new URLSearchParams(Object.entries(params));
           console.log('urlParams',urlParams)
           const data = await this.serviceAPI.get('/product',urlParams)
           runInAction(() => {
               this.productList = this.productList.concat(data);
               this.productResponse = data
           });
       } catch (error) {
           runInAction(() => {
               this.status = "error";
           });
       }
   }

   delete(item) {
      this.productList.splice(this.productList.indexOf(item), 1);
   }

   deleteProducts = async (item) => {
       try {
           const data = await this.serviceAPI.delete('/product',item.id)
           console.log('deleteProducts',JSON.stringify(data))
           if(data.success==1){
            this.delete(item)
           }
       } catch (error) {
           runInAction(() => {
               this.status = "error";
           });
       }
   }
}
