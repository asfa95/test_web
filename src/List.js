import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { PropTypes } from 'prop-types'; 
import Item from './Item';
import InfiniteScroll from 'react-infinite-scroll-component';

const List = observer(class List extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired
  };

  fetchMoreData = () => {
    var page = this.props.store.page +=1
    console.log('page',page)
    this.props.store.getProducts(page);
  };

  componentDidMount() {
        this.props.store.getProducts(this.props.store.page);
  }

  render() {
    return (
      <div className=''>
          <InfiniteScroll
            dataLength={this.props.store.productList.length}
            next={this.fetchMoreData}
            hasMore={(this.props.store.productResponse.length> 0 ? true:false)}
            loader={<center className='p-4'>  Memuat...</center>}
            
            endMessage={
            <p style={{ textAlign: "center" }}>
              <span>Tidak ada data lagi</span>
            </p>
          }
        >
        <div className="row p-3">
          {this.props.store.productList.map((item) => (
            <div key={item.id} className="col-6 col-sm-6 col-md-4 col-lg-3  mt-3">
              <Item store={this.props.store} todo={item} />
            </div>
          ))}
        </div>
        </InfiniteScroll>
      </div>
    );
  }
});

export default List;
