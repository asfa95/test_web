import logo from './logo.svg';
import './App.css';
import { makeAutoObservable, autorun } from "mobx"

import { TodoStore } from './Store/TodoProduct';
import List from './List';
import { observer } from 'mobx-react';
const todoStore = new TodoStore();

function App() {
  return (
      <div>
        <List store={todoStore} />
      </div>
  );
}

export default App;
